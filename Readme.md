## BigBitField ##

#### Overview ####
BigBitField is a simple and convenient templated class.

It designed to allow using bitwise operations on bit fields bigger than 64-bits, by performing a serial
of these operations on an array of primitive varaibles.

for example:
```c++
BigBitField<unsigned int, 3> bf96; // defines a 96-bits bit field
BigBitField<unsigned long long, 4> bf256; // defines a 256-bits bit field
```

#### Functionality ####

The class supports most of the operations required from a bit field:
 * Bitwise logic operators:
   `|`, `|=`, `&`, `&=`, `^`, `^=` and `~`.

 * Bitwise shift operators:
   `<<`, `<<=`, `>>` and `>>=`.
 
 * Comparison: `==` and `!=`. This class **does not** support _bigger than_ or _less than_ comparison.
 

Initializtion:
```c++
// uninitialized 96 bits bit-field
BigBitField<unsigned int, 3> a();

// the variable 0x000000000000000012345678
BigBitField<unsigned int, 3> b(0x12345678);

// the varaible 0x00112233445566778899aabb
BigBitField<unsigned int, 3> c({ 0x00112233, 0x44556677, 0x8899aabb });

// the varaible 0x00112233445566778899aabb
BigBitField<unsigned int, 3> d(c);

// the varaible 0x000000000000000000000000
auto zero = BigBitField<unsigned int, 3>::Zero();

// the varaible 0x000000000000000000000001
auto one = BigBitField<unsigned int, 3>::One();
```
