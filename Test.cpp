#include "BigBitField.h"
#include <iostream>
#include <assert.h>

int main()
{
	std::cout << "BigBitField Tests:" << std::endl;

	typedef BigBitField<unsigned char, 3> bitfield24;
	assert(bitfield24::Length() == 24);
	assert(sizeof(bitfield24) == 3);

	const bitfield24 bf24({ 0b00001111, 0b00110011, 0b01010101 });
	bitfield24 bf24_1, bf24_2, bf24_3;

	// operator[] //
	assert(bf24[0] == 0b00001111 && bf24[1] == 0b00110011 && bf24[2] == 0b01010101);

	//////////////
	// shifting //
	//////////////

	bf24_1 = bf24 << 0;
	bf24_2 = bf24 >> 0;
	assert(bf24_1 == bf24);
	assert(bf24_2 == bf24);

	bf24_1 = bf24 << 8;
	bf24_2 = bf24 >> 8;
	assert(bf24_1 == bitfield24({ 0b00110011, 0b01010101, 0b00000000 }));
	assert(bf24_2 == bitfield24({ 0b00000000, 0b00001111, 0b00110011 }));

	bf24_1 = bf24 << 16;
	bf24_2 = bf24 >> 16;
	assert(bf24_1 == bitfield24({ 0b01010101, 0b00000000, 0b00000000 }));
	assert(bf24_2 == bitfield24({ 0b00000000, 0b00000000, 0b00001111 }));

	bf24_1 = bf24 << 24;
	bf24_2 = bf24 >> 24;
	assert(bf24_1 == bitfield24::Zero());
	assert(bf24_2 == bitfield24::Zero());

	bf24_1 = bf24 << 1;
	bf24_2 = bf24 >> 1;
	assert(bf24_1 == bitfield24({ 0b00011110, 0b01100110, 0b10101010 }));
	assert(bf24_2 == bitfield24({ 0b00000111, 0b10011001, 0b10101010 }));

	bf24_1 = bf24 << 9;
	bf24_2 = bf24 >> 9;
	assert(bf24_1 == bitfield24({ 0b01100110, 0b10101010, 0b00000000 }));
	assert(bf24_2 == bitfield24({ 0b00000000, 0b00000111, 0b10011001 }));

	bf24_1 = bf24 << 17;
	bf24_2 = bf24 >> 17;
	assert(bf24_1 == bitfield24({ 0b10101010, 0b00000000, 0b00000000 }));
	assert(bf24_2 == bitfield24({ 0b00000000, 0b00000000, 0b00000111 }));


	//////////////////////////////
	// shifting with assignment //
	//////////////////////////////

	bf24_1 = bf24;
	bf24_2 = bf24;

	bf24_1 <<= 2;
	bf24_2 >>= 2;
	assert(bf24_1 == bitfield24({ 0b00111100, 0b11001101, 0b01010100 }));
	assert(bf24_2 == bitfield24({ 0b00000011, 0b11001100, 0b11010101 }));

	bf24_1 <<= 8;
	bf24_2 >>= 8;
	assert(bf24_1 == bitfield24({ 0b11001101, 0b01010100, 0b00000000 }));
	assert(bf24_2 == bitfield24({ 0b00000000, 0b00000011, 0b11001100 }));

	bf24_1 >>= 16;
	bf24_2 <<= 16;
	assert(bf24_1 == bitfield24({ 0b00000000, 0b00000000, 0b11001101 }));
	assert(bf24_2 == bitfield24({ 0b11001100, 0b00000000, 0b00000000 }));

	bf24_1 <<= 18;
	bf24_2 >>= 18;
	assert(bf24_1 == bitfield24({ 0b00110100, 0b00000000, 0b00000000 }));
	assert(bf24_2 == bitfield24({ 0b00000000, 0b00000000, 0b00110011 }));

	///////////////////////
	// bitwise operators //
	// not, or, and, xor //
	///////////////////////

	assert(~bitfield24::Zero() == bitfield24({ 0b11111111, 0b11111111, 0b11111111 }));
	assert(~bitfield24::One() == bitfield24({ 0b11111111, 0b11111111, 0b11111110 }));
	assert(~bf24 == bitfield24({ 0b11110000, 0b11001100, 0b10101010 }));

	assert((bf24 ^ bf24) == bitfield24::Zero());
	assert((bf24 ^ ~bf24) == ~bitfield24::Zero());
	assert((bf24 | ~bf24) == ~bitfield24::Zero());
	assert((bf24 & ~bf24) == bitfield24::Zero());

	assert((bf24 | (bf24 << 8)) == bitfield24({ 0b00111111, 0b01110111, 0b01010101 }));
	assert((bf24 & (bf24 << 8)) == bitfield24({ 0b00000011, 0b00010001, 0b00000000 }));
	assert((bf24 ^ (bf24 << 8)) == bitfield24({ 0b00111100, 0b01100110, 0b01010101 }));

	///////////////////////
	// bitwise operators //
	// with assignments  //
	///////////////////////

	bf24_1 = bf24;
	bf24_2 = bf24;
	bf24_3 = bf24;
	
	bf24_1 |= (bf24 << 8);
	bf24_2 &= (bf24 << 8);
	bf24_3 ^= (bf24 << 8);

	assert(bf24_1 == bitfield24({ 0b00111111, 0b01110111, 0b01010101 }));
	assert(bf24_2 == bitfield24({ 0b00000011, 0b00010001, 0b00000000 }));
	assert(bf24_3 == bitfield24({ 0b00111100, 0b01100110, 0b01010101 }));


	typedef BigBitField<unsigned int, 4> bitfield128;
	assert(bitfield128::Length() == 128);

	bitfield128 bf128 = bitfield128::One();
	int i = 0;
	while (bf128)
	{
		assert(bf128 == bitfield128::One() << i);
		bf128 <<= 1;
		i++;
	}
	assert(i == bitfield128::Length());



	std::cout << "All tests has passed" << std::endl;
	std::cin.get();
}
