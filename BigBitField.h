#pragma once

template<typename T, size_t N>
class BigBitField
{
private:
	// primitives[0] = Most Significant Primitive
	// primitives[N-1] = Least Significant Primitive
	T primitives[N];

	// size in bits of one primitive element
	static constexpr size_t PRIMITIVE_SIZE = sizeof(T) * 8;

	// size in bits of the whole register
	static constexpr size_t BIT_FIELD_SIZE = PRIMITIVE_SIZE * N;

	static void ShiftLeftInPlace(BigBitField& bitfield, size_t k)
	{
		// the implemnetation is based on this answer: https://stackoverflow.com/a/5996635
		// but instead of make it recursive we compute the amoun of primitive elements that
		// can be skipped and set to zero.
		
		size_t skip = k / PRIMITIVE_SIZE; // primitives that can be skipped and reset to zero
		size_t shift = k % PRIMITIVE_SIZE; // the actual amount of bites that need to be shifted

		if (shift != 0)
		{
			for (int p = 0; p < N - skip - 1; ++p)
				bitfield.primitives[p] = (bitfield.primitives[p + skip] << shift) | (bitfield.primitives[p + skip + 1] >> (PRIMITIVE_SIZE - shift));
			bitfield.primitives[N - skip - 1] = (bitfield[N - 1] << shift);
		}
		else
		{
			// edge case since shifting (PRIMITIVE_SIZE - 0) has undefined behaviour. https://stackoverflow.com/a/7471843
			for (int p = 0; p < N - skip; ++p)
				bitfield.primitives[p] = bitfield.primitives[p + skip];

		}
		for (int p = N-skip; p < N; ++p)
			bitfield.primitives[p] = 0;
	}

	static void ShiftRightInPlace(BigBitField& bitfield, size_t k)
	{
		// the implemnetation is based on this answer: https://stackoverflow.com/a/5996635
		// but instead of make it recursive we compute the amoun of primitive elements that
		// can be skipped and set to zero.

		size_t skip = k / PRIMITIVE_SIZE; // primitives that can be skipped and reset to zero
		size_t shift = k % PRIMITIVE_SIZE;

		if (shift != 0)
		{

			for (int p = N; p > skip + 1; --p)
				bitfield.primitives[p - 1] = (bitfield.primitives[p - 1 - skip] >> shift) | (bitfield.primitives[p - 1 - skip - 1] << (PRIMITIVE_SIZE - shift));
			bitfield.primitives[0+skip] = bitfield.primitives[0] >> shift;
		}
		else
		{
			// edge case since shifting (PRIMITIVE_SIZE - 0) has undefined behaviour. https://stackoverflow.com/a/7471843
			for (int p = N; p > skip; --p)
				bitfield.primitives[p - 1] = bitfield.primitives[p - skip - 1];

		}
		for (int p = 0; p < skip; ++p)
			bitfield.primitives[p] = 0;
	}

public:
	
	//////////////////
	// Constructors //
	//////////////////

	BigBitField() {}

	// construct BigBitField with leaset significant primitive
	BigBitField(T lsp)
	{
		for (int p = 0; p < N-1; ++p)
			primitives[p] = 0;
		primitives[N - 1] = lsp;
	}

	// construct BigBitField with an array of primitives
	BigBitField(const T(&primitives)[N])
	{
		for (int p = 0; p < N; ++p)
			this->primitives[p] = primitives[p];
	}

	// copy constructor
	BigBitField(const BigBitField & other)
	{
		for (int p = 0; p < N; ++p)
			this->primitives[p] = other.primitives[p];
	}

	/////////////
	// Getters //
	/////////////

	// length in bits of this BigBitField
	static constexpr size_t Length()
	{
		return BIT_FIELD_SIZE;
	}

	T operator[](int i) const
	{
		return primitives[i];
	}

	// return the instance 0x000...000
	static const BigBitField& Zero()
	{
		static const BigBitField ZERO(0);
		return ZERO;
	}
	
	// return the instance 0x000...001
	static const BigBitField& One()
	{
		static const BigBitField ONE(1);
		return ONE;
	}

	////////////////
	// Comparison //
	////////////////

	bool operator!=(const BigBitField& other) const
	{
		for (int p = 0; p < N; ++p)
			if (this->primitives[p] != other.primitives[p])
				return true;

		return false;
	}

	bool operator==(const BigBitField& other) const
	{
		for (int p = 0; p < N; ++p)
			if (this->primitives[p] != other.primitives[p])
				return false;

		return true;
	}

	operator bool()
	{
		return *this != Zero();
	}

	////////////////////////
	// Bitwise Arithmetic //
	////////////////////////

	BigBitField operator&(const BigBitField& other) const
	{
		BigBitField result;
		for (int p = 0; p < N; ++p)
			result.primitives[p] = this->primitives[p] & other.primitives[p];
		return result;
	}

	BigBitField operator|(const BigBitField& other) const
	{
		BigBitField result;
		for (int p = 0; p < N; ++p)
			result.primitives[p] = this->primitives[p] | other.primitives[p];
		return result;
	}

	BigBitField operator^(const BigBitField& other) const
	{
		BigBitField result;
		for (int p = 0; p < N; ++p)
			result.primitives[p] = this->primitives[p] ^ other.primitives[p];
		return result;
	}

	BigBitField operator~() const
	{
		BigBitField result;
		for (int p = 0; p < N; ++p)
			result.primitives[p] = ~this->primitives[p];
		return result;
	}

	BigBitField operator<<(size_t k) const
	{
		k = (k > BIT_FIELD_SIZE ? BIT_FIELD_SIZE : k);
		BigBitField result(*this);
		ShiftLeftInPlace(result, k);
		return result;
	}

	BigBitField operator>>(size_t k) const
	{
		k = (k > BIT_FIELD_SIZE ? BIT_FIELD_SIZE : k);
		BigBitField result(*this);
		ShiftRightInPlace(result, k);
		return result;
	}
	
	////////////////////////
	// Bitwise Assignment //
	////////////////////////

	BigBitField& operator&=(const BigBitField& other)
	{
		for (int p = 0; p < N; ++p)
			this->primitives[p] &= other.primitives[p];
		return *this;
	}

	BigBitField& operator|=(const BigBitField& other)
	{
		for (int p = 0; p < N; ++p)
			this->primitives[p] |= other.primitives[p];
		return *this;
	}

	BigBitField& operator^=(const BigBitField& other)
	{
		for (int p = 0; p < N; ++p)
			this->primitives[p] ^= other.primitives[p];
		return *this;
	}

	BigBitField& operator<<=(size_t k)
	{
		k = (k > BIT_FIELD_SIZE ? BIT_FIELD_SIZE : k);
		ShiftLeftInPlace(*this, k);
		return *this;
	}

	BigBitField& operator>>=(size_t k)
	{
		k = (k > BIT_FIELD_SIZE ? BIT_FIELD_SIZE : k);
		ShiftRightInPlace(*this, k);
		return *this;
	}

};
